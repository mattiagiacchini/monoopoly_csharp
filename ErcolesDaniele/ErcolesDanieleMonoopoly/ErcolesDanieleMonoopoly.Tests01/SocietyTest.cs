using System;
using ErcolesDanieleMonoopoly.tile.purchasable;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ErcolesDanieleMonoopoly.tile.purchasable.Tests
{
    [TestClass]
    public partial class SocietyTest
    {

        [TestMethod]
        public void TestTile() {
            ITile tile = new TileImpl.Builder()
                                   .Name("Via")
                                   .Deck(false)
                                   .Buildable(false)
                                   .Purchasable(true)
                                   .Category(Category.STATION)
                                   .Build();
            Assert.IsTrue(tile.GetName().Equals("Via"));
            Assert.IsFalse(tile.IsDeck());
            Assert.IsFalse(tile.IsBuildable());
            Assert.IsTrue(tile.IsPurchasable());
            Assert.IsTrue(tile.GetCategory().Equals(Category.STATION));
            Assert.IsTrue(tile is TileImpl, "errore di tipo");
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void TestSocietyWrongCompositionOne()
        {
            ITile tile = new TileImpl.Builder()
                                    .Name("Società")
                                    .Deck(false)
                                    .Buildable(false)
                                    .Purchasable(true)
                                    .Category(Category.SOCIETY)
                                    .Build();

            IPurchasable purch = new Society.Builder()
                                            //.Tile(tile)
                                            .mortgage(10.0)
                                            .Sales(20.0)
                                            .multiplierLevelOne(1.0)
                                            .multiplierLevelTwo(2.0)
                                            .SupplierDiceResult(() => 5)
                                            .FunNumOfCatOwned((x) => 2)
                                            .build();
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void TestSocietyWrongCompositionTwo()
        {
            ITile tile = new TileImpl.Builder()
                                    .Name("Società")
                                    .Deck(false)
                                    .Buildable(false)
                                    .Purchasable(true)
                                    .Category(Category.SOCIETY)
                                    .Build();

            IPurchasable purch = new Society.Builder()
                                            .Tile(tile)
                                            .mortgage(10.0)
                                            .Sales(20.0)
                                            //.multiplierLevelOne(1.0)
                                            .multiplierLevelTwo(2.0)
                                            .SupplierDiceResult(() => 5)
                                            .FunNumOfCatOwned((x) => 2)
                                            .build();
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void TestSocietyWrongCompositionThree()
        {
            ITile tile = new TileImpl.Builder()
                                    .Name("Probability")
                                    .Deck(true)
                                    .Buildable(false)
                                    .Purchasable(false)
                                    .Category(Category.PROBABILITY)
                                    .Build();

            IPurchasable purch = new Society.Builder()
                                            .Tile(tile)
                                            .mortgage(10.0)
                                            .Sales(20.0)
                                            .multiplierLevelOne(1.0)
                                            .multiplierLevelTwo(2.0)
                                            .SupplierDiceResult(() => 5)
                                            .FunNumOfCatOwned((x) => 2)
                                            .build();
        }

        [TestMethod]
        public void TestSociety()
        {
            
            ITile tile = new TileImpl.Builder()
                                    .Name("Società")
                                    .Deck(false)
                                    .Buildable(false)
                                    .Purchasable(true)
                                    .Category(Category.SOCIETY)
                                    .Build();

            IPurchasable purch = new Society.Builder()
                                            .Tile(tile)
                                            .mortgage(10.0)
                                            .Sales(20.0)
                                            .multiplierLevelOne(1.0)
                                            .multiplierLevelTwo(2.0)
                                            .SupplierDiceResult(() => 5)
                                            .FunNumOfCatOwned((x) => {
                                                if (x == 2) {
                                                    return 2;
                                                }
                                                return 1;
                                            })
                                            .build();
            
            purch.SetOwner(2);
            Assert.IsTrue(purch.GetOwner() == 2, "proprietario errato!");
            Assert.IsTrue(purch is Society, "errore di tipo");
            Assert.IsTrue(purch.GetMortgageValue() == 10.0);
            Assert.AreEqual(purch.GetLeaseValue(), 10.0);
            purch.SetOwner(203298);
            Assert.AreEqual(purch.GetLeaseValue(), 5.0);
            purch.SetQuotation(3.0);
            Assert.AreEqual(purch.GetLeaseValue(), 15.0);
            purch.SetOwner(null);
            Assert.IsNull(purch.GetOwner());
            Assert.AreEqual(purch.GetLeaseValue(), 0.0);
        }
    }
}
