// <copyright file="SocietyTest.cs">Copyright ©  2020</copyright>
using System;
using ErcolesDanieleMonoopoly.tile.purchasable;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ErcolesDanieleMonoopoly.tile.purchasable.Tests
{
    /// <summary>Questa classe contiene unit test con parametri per Society</summary>
    [PexClass(typeof(Society))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class SocietyTest
    {
        /// <summary>Test stub per GetLeaseValue()</summary>
        [PexMethod]
        internal double GetLeaseValueTest([PexAssumeUnderTest]Society target)
        {
            double result = target.GetLeaseValue();
            return result;
            // TODO: aggiungere asserzioni a metodo SocietyTest.GetLeaseValueTest(Society)
        }
    }
}
