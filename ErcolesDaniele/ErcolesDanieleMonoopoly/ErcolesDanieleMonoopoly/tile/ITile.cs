﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErcolesDanieleMonoopoly.tile
{
    public interface ITile
    {
        bool IsPurchasable();
        bool IsDeck();
        bool IsBuildable();
        string GetName();
        Category GetCategory();
    }

    public enum Category
    {
        START,
        JAIL,
        GO_TO_JAIL,
        FREE_PARKING,
        BROWN,
        LIGHT_BLUE,
        PINK,
        ORANGE,
        RED,
        YELLOW,
        GREEN,
        BLUE,
        UNEXPECTED,
        PROBABILITY,
        CALAMITY,
        SOCIETY,
        STATION
    };

}
