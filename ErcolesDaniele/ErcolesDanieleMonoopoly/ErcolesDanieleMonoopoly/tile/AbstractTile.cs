﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErcolesDanieleMonoopoly.tile
{
    abstract class AbstractTile : ITile
    {
        private readonly ITile _decorated;

        public AbstractTile(ITile tile)
        {
            if (tile == null)
            {
                throw new ArgumentNullException("Tile to decore cannot has null value");
            }
            this._decorated = tile;
        }

        public Category GetCategory()
        {
            return this._decorated.GetCategory();
        }

        public string GetName()
        {
            return this._decorated.GetName();
        }

        public bool IsBuildable()
        {
            return this._decorated.IsBuildable();
        }

        public bool IsDeck()
        {
            return this._decorated.IsDeck();
        }

        public bool IsPurchasable()
        {
            return this._decorated.IsPurchasable();
        }
    }
}
