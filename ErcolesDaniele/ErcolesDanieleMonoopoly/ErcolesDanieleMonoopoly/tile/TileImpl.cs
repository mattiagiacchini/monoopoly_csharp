﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErcolesDanieleMonoopoly.tile
{
    class TileImpl : ITile
    {
        private readonly String _name;
        private readonly Category _category;
        private readonly bool _deck;
        private readonly bool _purchasable;
        private readonly bool _buildable;

        public class Builder
        {
            private String _name;
            private Category _cat;
            private bool _deck;
            private bool _deckSetted;
            private bool _purchasable;
            private bool _purchasableSetted;
            private bool _buildable;
            private bool _buildableSetted;

            public Builder()
            {
                this._name = null;
                this._cat = 0;
                this._buildableSetted = false;
                this._deckSetted = false;
                this._purchasableSetted = false;
            }

            public Builder Name(String name)
            {
                this.ObjRequired(name, "name");
                this._name = name;
                return this;
            }

            public Builder Category(Category cat)
            {
                if (cat == 0)
                {
                    throw new ArgumentException("[TileImpl] category cannot has zero value");
                }
                this._cat = cat;
                return this;
            }

            public Builder Deck(bool deck)
            {
                this._deck = deck;
                this._deckSetted = true;
                return this;
            }

            public Builder Buildable(bool buildable)
            {
                this._buildable = buildable;
                this._buildableSetted = true;
                return this;
            }

            public Builder Purchasable(bool purchasable)
            {
                this._purchasable = purchasable;
                this._purchasableSetted = true;
                return this;
            }

            public TileImpl Build() {
                if (this._purchasableSetted == true
                   && this._deckSetted == true
                   && this._buildableSetted == true
                   && this._cat != 0
                   && this._name != null)
                {
                    return new TileImpl(this._name, this._cat, this._purchasable, this._deck, this._buildable);
                }
                throw new ArgumentNullException("[Builder-TileImpl] not all parameters are setted");
            }

            private void ObjRequired(Object obj, string text)
            {
                if (obj == null)
                {
                    throw new ArgumentNullException("[Builder-TileImpl] " + text + " cannot has null value");
                }
            }
        }

        private TileImpl(String name, Category cat, bool purchasable, bool deck, bool buildable)
        {
            this._name = name;
            this._category = cat;
            this._purchasable = purchasable;
            this._deck = deck;
            this._buildable = buildable;
        }

        public bool IsPurchasable()
        {
            return this._purchasable;
        }

        public bool IsDeck()
        {
            return this._deck;
        }

        public bool IsBuildable()
        {
            return this._buildable;
        }

        public string GetName()
        {
            return this._name;
        }

        public Category GetCategory()
        {
            return this._category;
        }
    }
}
