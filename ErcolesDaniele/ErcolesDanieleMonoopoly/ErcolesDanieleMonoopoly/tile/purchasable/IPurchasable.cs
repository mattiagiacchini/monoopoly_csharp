﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErcolesDanieleMonoopoly.tile.purchasable
{
    interface IPurchasable : ITile
    {
        double Mortgage();
        
        bool IsMortgage();

        void RemoveMortgage();

        double GetLeaseValue();

        double GetSalesValue();

        double GetMortgageValue();

        double GetCostToRemoveMortgage();

        double GetQuotation();
        
        void SetQuotation(double quotation);
        
        void SetOwner(int? newOwnerIdentify);
        
        int? GetOwner();
        
        Dictionary<int, double> GetLeaseList();
    }
}
