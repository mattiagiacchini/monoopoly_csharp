﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErcolesDanieleMonoopoly.tile.purchasable
{
    abstract class AbstractPurchasable : AbstractTile,IPurchasable
    {
        private const double PRECENTAGE_TO_REMOVE_MORTGAGE = 1.1;
        private const double BASE_QUOTATION = 1.0;

        private readonly double morgageValue;
        private readonly double salesValue;

        private int? _ownerIdentify;
        private double _quotation;
        private bool _mortgageStatus;

        public AbstractPurchasable(ITile tile,
                                   double mortgageValue,
                                   double salesValue) : base(tile)
        {
            if (!tile.IsPurchasable())
            {
                throw new ArgumentException("[AbstractPurchasable] Tile isn't Purchasable");
            }
            this._mortgageStatus = false;
            this._quotation = AbstractPurchasable.BASE_QUOTATION;
            this.morgageValue = mortgageValue;
            this.salesValue = salesValue;
            this._ownerIdentify = null;
        }

        public new Category GetCategory()
        {
            return base.GetCategory();
        }

        public double GetCostToRemoveMortgage()
        {
            return this.applyQuotationOnValue(this.morgageValue 
                * AbstractPurchasable.PRECENTAGE_TO_REMOVE_MORTGAGE);
        }

        public abstract Dictionary<int, double> GetLeaseList();

        public abstract double GetLeaseValue();

        public double GetMortgageValue()
        {
            return this.applyQuotationOnValue(this.morgageValue);
        }

        public new string GetName()
        {
            return base.GetName();
        }

        public int? GetOwner()
        {
            return this._ownerIdentify;
        }

        public double GetQuotation()
        {
            return this._quotation;
        }

        public double GetSalesValue()
        {
            return this.applyQuotationOnValue(this.salesValue);
        }

        public new bool IsBuildable()
        {
            return base.IsBuildable();
        }

        public new bool IsDeck()
        {
            return base.IsDeck();
        }

        public bool IsMortgage()
        {
            return this._mortgageStatus;
        }

        public new bool IsPurchasable()
        {
            return base.IsPurchasable();
        }

        public double Mortgage()
        {
            this._mortgageStatus = true;
            return this.GetMortgageValue();
        }

        public void RemoveMortgage()
        {
            this._mortgageStatus = false;
        }

        public void SetOwner(int? newOwnerIdentify)
        {
            this._ownerIdentify = newOwnerIdentify;
        }

        public void SetQuotation(double quotation)
        {
            this._quotation = quotation;
        }
        protected double applyQuotationOnValue(double value)
        {
            return value * this.GetQuotation();
        }
    }
}
