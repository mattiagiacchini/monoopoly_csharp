﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErcolesDanieleMonoopoly.tile.purchasable
{
    class Society : AbstractPurchasable
    {
        private const double NO_LEASE = 0.0;
        private const int LEVEL_ONE = 1;
        private const int LEVEL_TWO = 2;

        private readonly double _multiplierLevelOne;
        private readonly double _multiplierLevelTwo;
        private readonly Func<int>  _supplierDiceResult;
        private readonly Func<int, int> _functionGetNumOfSocietyOwned;

        public class Builder
        {
            private ITile _decorated;
            private double? _mortgageValue;
            private double? _salesValue;
            private double? _multiplierLevelOne;
            private double? _multiplierLevelTwo;
            private Func<int> _supplierDiceResult;
            private Func<int, int> _functionGetNumOfSocietyOwned;
            
            public Builder()
            {
                this._decorated = null;
                this._mortgageValue = null;
                this._salesValue = null;
                this._multiplierLevelOne = null;
                this._multiplierLevelTwo = null;
                this._supplierDiceResult = null;
                this._functionGetNumOfSocietyOwned = null;
            }

            public Builder Tile(ITile decorated)
            {
                if (decorated.GetCategory() != Category.SOCIETY) {
                    throw new ArgumentException("the Tile isn't a Society");
                }
                this._decorated = decorated;
                return this;
            }

            public Builder SupplierDiceResult(Func<int> supplierDiceResult) {
                if (supplierDiceResult == null) {
                    throw new ArgumentNullException("the supplier of dice cannot has null value!");
                }
                this._supplierDiceResult = supplierDiceResult;
                return this;
            }

            public Builder FunNumOfCatOwned(Func<int,int> function)
            {
                if (function == null) {
                    throw new ArgumentNullException("the Function of category Owned cannot has null value");
                }
                this._functionGetNumOfSocietyOwned = function;
                return this;
            }

            public Builder mortgage(double mortgageValue)
            {
                this.DoubleChecker(mortgageValue, "mortgage value wrong format!");
                this._mortgageValue = mortgageValue;
                return this;
            }

            public Builder Sales(double salesValue)
            {
                this.DoubleChecker(salesValue, "sales value wrong format!");
                this._salesValue = salesValue;
                return this;
            }

            public Builder multiplierLevelOne(double multiplier)
            {
                this.DoubleChecker(multiplier, "multiplier level one wrong format");
                this._multiplierLevelOne = multiplier;
                return this;
            }

            public Builder multiplierLevelTwo(double multiplier)
            {
                this.DoubleChecker(multiplier, "multiplier level one wrong format");
                this._multiplierLevelTwo = multiplier;
                return this;
            }

            public Society build() {
                this.NullCheck<ITile>(this._decorated, "Tile");
                this.NullCheck<double?>(this._mortgageValue, "Mortgage's Value");
                this.NullCheck<double?>(this._salesValue, "Sale's value");
                this.NullCheck<double?>(this._multiplierLevelOne, "Multiplier levele one");
                this.NullCheck<double?>(this._multiplierLevelTwo, "Multiplier levele two");
                this.NullCheck<Func<int>>(this._supplierDiceResult, "Suplier of dices");
                this.NullCheck<Func<int, int>>(this._functionGetNumOfSocietyOwned, 
                    "function to know the number of society owned");

                return new Society(this._decorated,
                                   (double)this._mortgageValue,
                                   (double)this._salesValue,
                                   (double)this._multiplierLevelOne,
                                   (double)this._multiplierLevelTwo,
                                   this._supplierDiceResult,
                                   this._functionGetNumOfSocietyOwned);
            }

            private void NullCheck<T>(T obj, string str) {
                if (obj == null) {
                    throw new ArgumentNullException("[Society-Builder] "
                        + str + " isn't setted!");
                }
            }

            private void DoubleChecker(double value, string str) {
                if (Double.IsNaN(value) || Double.IsInfinity(value)) {
                    throw new ArgumentException(str);
                }
            }
        }

        private Society(ITile tile, double mortgageValue, 
                        double salesValue, double multiplierLevelOne,
                        double multiplierLevelTwo,
                        Func<int> supplierDiceResult,
                        Func<int,int> functionGetNumOfSocietyOwned) 
                      : base(tile, mortgageValue, salesValue)
        {
            this._multiplierLevelOne = multiplierLevelOne;
            this._multiplierLevelTwo = multiplierLevelTwo;
            this._supplierDiceResult = supplierDiceResult;
            this._functionGetNumOfSocietyOwned =
                functionGetNumOfSocietyOwned;
        }

        public override Dictionary<int, double> GetLeaseList()
        {
            Dictionary<int, double> dict = new Dictionary<int, double>();
            dict.Add(Society.LEVEL_ONE, base.applyQuotationOnValue(this._multiplierLevelOne));
            dict.Add(Society.LEVEL_TWO, base.applyQuotationOnValue(this._multiplierLevelTwo));
            return dict;
        }

        public override double GetLeaseValue()
        {
            if (base.GetOwner() != null) {
                int nSociety = this._functionGetNumOfSocietyOwned((int)base.GetOwner());

                if (Society.LEVEL_ONE.Equals(nSociety)) {
                    return base.applyQuotationOnValue(this._multiplierLevelOne * this._supplierDiceResult());
                } else if (Society.LEVEL_TWO.Equals(nSociety)) {
                    return base.applyQuotationOnValue(this._multiplierLevelTwo * this._supplierDiceResult());
                }
            }
            return Society.NO_LEASE;
        }
    }
}
