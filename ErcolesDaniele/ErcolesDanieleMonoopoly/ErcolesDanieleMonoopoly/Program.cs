﻿using System;
using ErcolesDanieleMonoopoly.tile;
using ErcolesDanieleMonoopoly.tile.purchasable;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErcolesDanieleMonoopoly
{
    class Program
    {
        static void Main(string[] args)
        {
            ITile tile = new TileImpl.Builder()
                                     .Name("Società")
                                     .Deck(false)
                                     .Buildable(false)
                                     .Purchasable(true)
                                     .Category(Category.SOCIETY)
                                     .Build();

            IPurchasable purch = new Society.Builder()
                                            .Tile(tile)
                                            .mortgage(10.0)
                                            .Sales(20.0)
                                            .multiplierLevelOne(1.0)
                                            .multiplierLevelTwo(2.0)
                                            .SupplierDiceResult(() => 5)
                                            .FunNumOfCatOwned((x) => 2)
                                            .build();

            purch.SetOwner(2);
            Console.WriteLine("categoria: " + purch.GetCategory());
            Console.WriteLine("valore d'ipoteca: " + purch.GetMortgageValue());
            Console.WriteLine("ID proprietario: " + purch.GetOwner());
            Console.WriteLine("valore d'affitto: " + purch.GetLeaseValue());
            purch.SetOwner(null);
            Console.WriteLine(purch.GetOwner());
            Console.WriteLine(purch.GetLeaseValue());


        }
    }
}
