﻿using GiacchiniMattia_Monoopoly;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestGiacchini
{
    [TestClass]
    public class TestPlayerManager
    {
        IPlayerBuilder builder = new PlayerBuilderImpl();
        IPlayerManager manager;

        [TestInitialize]
        public void Initialize()
        {
            IPlayer builderPlayer = builder.PlayerID(2).Name("Mattia").State(States.IN_GAME).LeavePrisonForFree(false).Balance(500.00).Position(23).Build();
            manager = new PlayerManagerImpl(2, builderPlayer);
        }

        [TestMethod]
        public void TestBalanceMovements()
        {
            manager.CollectMoney(500.0);
            Assert.AreEqual(1000.0, manager.GetPlayer().Balance);
            manager.PayMoney(1500.0);
            Assert.AreEqual(-500.0, manager.GetPlayer().Balance);
        }

        [TestMethod]
        public void TestPlayerMovement()
        {
            manager.MovePlayer(50);
            Assert.IsTrue(manager.GetPlayer().Position == 33);
            manager.GoToPosition(25);
            Assert.IsTrue(manager.GetPlayer().Position == 25);
        }

        [TestMethod]
        public void TestPlayerPrisonement()
        {
            manager.GoToPrison();
            Assert.IsTrue(manager.GetPlayer().Position == 10);
            Assert.AreEqual(manager.GetPlayer().State, States.PRISONED);
            manager.LeavePrison();
            Assert.AreEqual(manager.GetPlayer().State, States.IN_GAME);
            
            manager.GoToPosition(30);
            Assert.AreEqual(manager.GetPlayer().State, States.PRISONED);
            manager.LeavePrison();
            Assert.AreEqual(manager.GetPlayer().State, States.IN_GAME);

            manager.GetPlayer().PrisonCard = true;
            manager.GoToPrison();
            Assert.IsTrue(manager.GetPlayer().Position == 10);
            Assert.AreEqual(manager.GetPlayer().State, States.IN_GAME);
        }

        [TestMethod]
        public void TestGiveUp()
        {
            manager.GiveUp();
            Assert.IsTrue(manager.GetPlayer().State == States.BROKE);
        }
    }
}
