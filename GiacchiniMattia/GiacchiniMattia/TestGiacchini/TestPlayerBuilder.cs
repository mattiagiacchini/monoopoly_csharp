using GiacchiniMattia_Monoopoly;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestGiacchini
{
    [TestClass]
    public class TestPlayerBuilder
    {
        [TestMethod]
        public void TestCreation()
        {
            IPlayerBuilder builder = new PlayerBuilderImpl();
            PlayerImpl myPlayer = new PlayerImpl(11, "Mattia", 500.00, 23, States.IN_GAME, true);
            IPlayer builderPlayer = builder.PlayerID(11).Name("Mattia").State(States.IN_GAME).LeavePrisonForFree(true).Balance(500.00).Position(23).Build();
            Assert.AreEqual(myPlayer.Name, builderPlayer.Name);
            Assert.AreEqual(myPlayer.Balance, builderPlayer.Balance);
            Assert.AreEqual(myPlayer.PlayerID, builderPlayer.PlayerID);
            Assert.AreEqual(myPlayer.Position, builderPlayer.Position);
            Assert.AreEqual(myPlayer.State, builderPlayer.State);
            Assert.AreEqual(myPlayer.PrisonCard, builderPlayer.PrisonCard);
        }

        [TestMethod]
        public void TestPartialCreation()
        {
            IPlayerBuilder builder = new PlayerBuilderImpl();
            IPlayer builderPlayer = builder.PlayerID(11).Name("Mattia").Balance(500.00).Build();
            Assert.AreEqual("Mattia", builderPlayer.Name);
            Assert.AreEqual(500.0, builderPlayer.Balance);
            Assert.AreEqual(11, builderPlayer.PlayerID);
            Assert.AreEqual(0, builderPlayer.Position);
            Assert.AreEqual(States.IN_GAME, builderPlayer.State);
            Assert.AreEqual(false, builderPlayer.PrisonCard);
        }
    }
}
