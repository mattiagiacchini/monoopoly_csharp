﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiacchiniMattia_Monoopoly
{
    public class PlayerBuilderImpl : IPlayerBuilder
    {
        private int playerID;
        private String name;
        private Double balance;
        private int position;
        private States state = States.IN_GAME;
        private Boolean prisonCard;

        public IPlayerBuilder Balance(double balance)
        {
            this.balance = balance;
            return this;
        }

        public IPlayer Build()
        {
            return new PlayerImpl(this.playerID, this.name, this.balance, this.position, this.state, this.prisonCard);
        }
        public IPlayerBuilder LeavePrisonForFree(bool leave)
        {
            this.prisonCard = leave;
            return this;
        }

        public IPlayerBuilder Name(string name)
        {
            this.name = name;
            return this;
        }

        public IPlayerBuilder PlayerID(int id)
        {
            this.playerID = id;
            return this;
        }

        public IPlayerBuilder Position(int position)
        {
            this.position = position;
            return this;
        }

        public IPlayerBuilder State(States state)
        {
            this.state = state;
            return this;
        }
    }

}
