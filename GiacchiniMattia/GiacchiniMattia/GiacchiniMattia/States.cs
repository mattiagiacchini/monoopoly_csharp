﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiacchiniMattia_Monoopoly
{
    public enum States
    {
        /**
         * This state means that the Player is in game.
         */
        IN_GAME,

        /**
         * This state means that the Player is in prison.
         */
        PRISONED,

        /**
         * This state means that the Player has payed the rent for the current
         * turn.
         */
        HAS_PAYED_RENT,

        /**
         * This state means that the Player have been eliminated.
         */
        BROKE
    }

}