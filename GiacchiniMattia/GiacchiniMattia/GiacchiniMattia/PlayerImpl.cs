﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiacchiniMattia_Monoopoly
{
    public class PlayerImpl : IPlayer
    {
        public int PlayerID { get; }
        public String Name { get; }
        public Double Balance { get; set; }
        public int Position { get; set; }
        public States State { get; set; }
        public Boolean PrisonCard { get; set; }

        public PlayerImpl(int id, String name, Double balance, int position, States state, Boolean prisonCard)
        {
            this.PlayerID = id;
            this.Name = name;
            this.Balance = balance;
            this.Position = position;
            this.State = state;
            this.PrisonCard = prisonCard;
        }

        public Boolean HasPrisonCard()
        {
            return this.PrisonCard;
        }

        public void UpdateBalance(double value)
        {
            this.Balance += value;
        }
    }
}
