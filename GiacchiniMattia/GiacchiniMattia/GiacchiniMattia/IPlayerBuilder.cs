﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiacchiniMattia_Monoopoly
{
    public interface IPlayerBuilder
    {
        IPlayerBuilder PlayerID(int id);
        
        IPlayerBuilder Name(String name);
        
        IPlayerBuilder Balance(Double balance);
        
        IPlayerBuilder Position(int position);
        
        IPlayerBuilder State(States state);
        
        IPlayerBuilder LeavePrisonForFree(Boolean leave);

        IPlayer Build();
    }
}
