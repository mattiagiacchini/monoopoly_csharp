﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiacchiniMattia_Monoopoly
{
    public class PlayerManagerImpl : IPlayerManager
    {
        private const int TURN_IN_PRISON = 3;

        private int PlayerManagerID { get; }
        private PlayerImpl player;
        private int PrisonTurnCounter { get; set; }

        public PlayerManagerImpl(int id, IPlayer player)
        {
            this.player = (PlayerImpl)player;
            this.PlayerManagerID = id;
        }
        
        public void CollectMoney(double amount)
        {
            this.player.UpdateBalance(amount);
        }

        public IPlayer GetPlayer()
        {
            return this.player;
        }

        public int GetPlayerManagerID()
        {
            return this.PlayerManagerID;
        }

        public int GetPrisonTurnCounter()
        {
            return this.PrisonTurnCounter;
        }

        public void GiveUp()
        {
            this.player.State = States.BROKE;
        }

        public void GoToPosition(int position)
        {
            if (position >= 0 && position < 40)
            {
                if (!this.IsInPrison())
                {
                    if (this.CheckGoToJail(position))
                    {
                        this.GoToPrison();
                    }
                    else
                    {
                        this.player.Position = position;
                    }
                }
            }
        }

        private bool CheckGoToJail(int position)
        {
            return position == 30; 
        }

        public void GoToPrison()
        {
            this.player.Position = 10;
            if (this.player.HasPrisonCard())
            {
                this.player.PrisonCard = false;
            }
            else
            {
                this.player.State = States.PRISONED;
                this.PrisonTurnCounter = 0;
            }
        }

        public bool IsBroken()
        { 
            return this.player.State == States.BROKE;
        }

        public bool IsInPrison()
        {
            return this.player.State == States.PRISONED;
        }

        public void LeavePrison()
        {
            this.PrisonTurnCounter = TURN_IN_PRISON;
            this.player.State = States.IN_GAME;
        }

        public void MovePlayer(int steps)
        {
            if (!this.IsInPrison())
            {
                this.LeavePrison();
                this.GoToPosition(this.NextPosition(steps));
            }
        }

        private int NextPosition(int steps)
        {
            return this.CheckOutOfBoard(this.player.Position + steps);
        }
         
        private int CheckOutOfBoard(int position)
        {
            if (position >= 40)
            {
                return position - 40;

            }
            else if (position < 0)
            {
                return position + 40;
            }
            else
            {
                return position;
            }
        }

        public void NewTurn()
        {
            this.PrisonTurnCounter++;
        }

        public void PayMoney(double amount)
        {
            this.player.UpdateBalance(-amount);
        }

        public void ResetPrisonCounter()
        {
            this.PrisonTurnCounter = 0;
        }
    }
}
