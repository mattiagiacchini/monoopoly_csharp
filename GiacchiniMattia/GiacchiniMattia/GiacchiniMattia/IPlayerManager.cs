﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiacchiniMattia_Monoopoly
{
    public interface IPlayerManager
    {
        int GetPlayerManagerID();

        IPlayer GetPlayer();

        void MovePlayer(int steps);

        void GoToPosition(int position);

        void GiveUp();

        void PayMoney(Double amount);

        void CollectMoney(Double amount);

        void LeavePrison();

        Boolean IsInPrison();

        void NewTurn();

        int GetPrisonTurnCounter();

        Boolean IsBroken();

        void ResetPrisonCounter();

        void GoToPrison();
    }
}