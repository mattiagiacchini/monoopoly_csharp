﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GiacchiniMattia_Monoopoly
{
   public interface IPlayer
    {
        public int PlayerID { get; }
        public String Name { get; }
        public Double Balance { get; set; }
        public int Position { get; set; }
        public States State { get; set; }
        public Boolean PrisonCard { get; set; }

        public void UpdateBalance(Double value);

        public Boolean HasPrisonCard();
    }
}
