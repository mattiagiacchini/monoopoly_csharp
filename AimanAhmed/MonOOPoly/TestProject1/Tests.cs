﻿using System;
using MonOOPoly;
using MonOOPoly.Properties;
using NUnit.Framework;

namespace TestProject1
{
    [TestFixture]
    public class Tests
    {
        private readonly ITurnManager _turnManager = new TurnManagerImpl();

        private void BuildPlayers() 
        { 
            IPlayerManager pM1 = new PlayerManagerImpl(0); 
            this._turnManager.GetPlayersList().Add(pM1);
            IPlayerManager pM2 = new PlayerManagerImpl(1); 
            this._turnManager.GetPlayersList().Add(pM2);
            IPlayerManager pM3 = new PlayerManagerImpl(2);
            this._turnManager.GetPlayersList().Add(pM3);
        }
        [Test]
        public void TestPassPlayer()
        { 
            this.BuildPlayers(); 
            this._turnManager.NextTurn(); 
            Assert.AreEqual(1,this._turnManager.GetCurrentPlayer());
            this._turnManager.NextTurn(); 
            Assert.AreEqual(2,this._turnManager.GetCurrentPlayer());
            this._turnManager.NextTurn(); 
            Assert.AreEqual(0,this._turnManager.GetCurrentPlayer());
        }
    }
}