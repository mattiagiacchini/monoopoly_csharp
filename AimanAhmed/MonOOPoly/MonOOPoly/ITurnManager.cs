using System.Collections.Generic;
using MonOOPoly.Properties;

namespace MonOOPoly
{
    public interface ITurnManager
    {
        IPlayerManager NextTurn();
        List<IPlayerManager> GetPlayersList();
        int GetCurrentPlayer();
        void SetRound();
        void SetCurrentId(int iD);
    }
}
