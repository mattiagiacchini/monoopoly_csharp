using System;
using System.Collections.Generic;

namespace MonOOPoly
{
    public interface IStartGame
    { 
        void CreateEngine();
        void SetName(Dictionary<int, string> name);
        void SetBalance(Dictionary<int, double> balance);
    }
}
