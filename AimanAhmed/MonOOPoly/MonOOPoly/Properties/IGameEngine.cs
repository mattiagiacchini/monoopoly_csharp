using System;
using System.Collections.Generic;

namespace MonOOPoly.Properties
{
    public interface IGameEngine
    {
        void CreatePlayers();
        IPlayerManager CurrentPlayer();
        List<IPlayerManager> PlayersList();
        string GetName(int iD);
        double GetBalance(int iD);
    }
}