using System.Collections.Generic;

namespace MonOOPoly.Properties
{
    public class TurnManagerImpl : ITurnManager
    {
        private int _currentPlayerId;
        private readonly List<IPlayerManager> playersList = new List<IPlayerManager>();
        private int _round;

        public TurnManagerImpl()
        {
            this._round = -1;
        }

        public IPlayerManager NextTurn()
        {
            int flag = 0;
            foreach (var pM in this.playersList)
            { 
                if (flag == 1) 
                { 
                    this.CurrentPlayerId = pM.GetPlayerManagerId(); 
                    return pM;
                }
                if (pM.GetPlayerManagerId() == this._currentPlayerId) 
                { 
                    flag = flag + 1;
                } 
            }
            this.CurrentPlayerId = 0;
            return this.playersList[0];
        }
        public int CurrentPlayerId
        {
            get { return this._currentPlayerId; }
            set { this._currentPlayerId = value; }
        }
        
        public int Round
        {
            get { return this._round; }
            set { this._round = value; }
        }

        public List<IPlayerManager> GetPlayersList()
        {
            return this.playersList;
        }

        public int GetCurrentPlayer()
        {
            return this._currentPlayerId;
        }

        public void SetRound()
        {
            this._round = this._round + 1;
        }

        public void SetCurrentId(int iD)
        {
            this._currentPlayerId = iD;
        }
    }
}