namespace MonOOPoly.Properties
{
    public class PlayerManagerImpl : IPlayerManager
    {
        private readonly int playerManagerId;

        public PlayerManagerImpl(int playerManagerId)
        {
            this.playerManagerId = playerManagerId;
        }

        public int GetPlayerManagerId()
        {
            return this.playerManagerId;
        }
    }
}