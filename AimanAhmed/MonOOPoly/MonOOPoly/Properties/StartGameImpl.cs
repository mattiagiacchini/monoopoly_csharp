using System.Collections.Generic;

namespace MonOOPoly.Properties
{
    public class StartGameImpl : IStartGame
    {
        private Dictionary<int, string> _name = new Dictionary<int, string>(); 
        private Dictionary<int, double> _balance = new Dictionary<int, double>(); 
        
        public void CreateEngine()
        {
            IGameEngine engine = new GameEngineImpl(this._name, this._balance);
            engine.CreatePlayers();
        }

        public void SetName(Dictionary<int, string> name)
        {
            this._name = name;
        }

        public void SetBalance(Dictionary<int, double> balance)
        {
            this._balance = balance;
        }

        public Dictionary<int, string> Name { get; set; }
        public Dictionary<int, double> Balance { get; set; }
    }
}

