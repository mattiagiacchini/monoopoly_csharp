using System;
using System.Collections;
using System.Collections.Generic;

namespace MonOOPoly.Properties
{
    public class GameEngineImpl : IGameEngine
    {
        private readonly Dictionary<int, string> name;
        private readonly Dictionary<int, double> balance;
        private readonly ITurnManager turnManager = new TurnManagerImpl();

        public GameEngineImpl(Dictionary<int, string> name, Dictionary<int, double> balance)
        {
            this.name = name;
            this.balance = balance;
        }

        private IPlayerManager CreatePlayer(int iD)
        {
            IPlayerManager pM = new PlayerManagerImpl(iD);
            return pM;
        }

        public void CreatePlayers()
        {
            this.turnManager.SetCurrentId(this.name.Values.Count - 1);
            IDictionaryEnumerator it = this.name.GetEnumerator();
            while (it.MoveNext()) { 
                this.turnManager.GetPlayersList().Add(this.CreatePlayer((int)it.Key));
            } 
        }

        public IPlayerManager CurrentPlayer()
        {
            foreach (var pM in this.turnManager.GetPlayersList()) 
            {
                if (pM.GetPlayerManagerId() == this.turnManager.GetCurrentPlayer()) 
                {
                    return pM;
                }
            } 
            return null;
        }

        public List<IPlayerManager> PlayersList()
        {
            return this.turnManager.GetPlayersList();
        }

        public string GetName(int iD)
        {
           if (this.name.ContainsKey(iD)) 
           { 
               return this.name[iD];
           } else 
           { 
               throw new ArgumentException("No player found");
           } 
        }

        public double GetBalance(int iD)
        {
            if (this.name.ContainsKey(iD))
            {
                return this.balance[iD];
            } else
            { 
                throw new ArgumentException("No player found");
            }
        }

        private void incRound()
        {
            this.turnManager.SetRound();
        }
    }
}