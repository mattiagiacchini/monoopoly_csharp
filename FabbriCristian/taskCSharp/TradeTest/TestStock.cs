﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using taskCSharp;

namespace TradeTest
{
    [TestClass]
    public class TestStock
    {
        [TestMethod]
        public void TestInit()
        {
            Purchasable one = new Purchasable("1", 10.0, Category.BLUE);
            Purchasable two = new Purchasable("2", 15.0, Category.BROWN);
            List<IPurchasable> list = new List<IPurchasable>
            {
                one,
                two
            };
            StockMarket stock = new StockMarket(list);

            Dictionary<Category, Double> initDict = new Dictionary<Category, double>
            {
                { Category.BLUE, 1.0 },
                { Category.BROWN, 1.0 },
                { Category.GREEN, 1.0 },
                { Category.LIGHT_BLUE, 1.0 },
                { Category.ORANGE, 1.0 },
                { Category.PINK, 1.0 },
                { Category.RED, 1.0 },
                { Category.SOCIETY, 1.0 },
                { Category.STATION, 1.0 },
                { Category.YELLOW, 1.0 }
            };
            Assert.IsTrue(stock.GetStockHistory().Count == 1);
        }
        
        [TestMethod]
        public void TestGen()
        {
            Purchasable one = new Purchasable("1", 10.0, Category.BLUE);
            Purchasable two = new Purchasable("2", 15.0, Category.BROWN);
            List<IPurchasable> list = new List<IPurchasable>
            {
                one,
                two
            };
            StockMarket stock = new StockMarket(list);
            stock.SetNewMarketValue();
            Assert.IsTrue(stock.GetStockHistory().Count == 2);
            Console.WriteLine(stock.GetStockHistory()[1]);
        }
    }
}
