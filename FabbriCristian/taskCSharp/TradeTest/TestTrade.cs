﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using taskCSharp;

namespace Testing
{
    [TestClass]
    public class TestTrade
    {
        [TestMethod]
        public void TestBuilderComplete()
        {
            TradeBuilder builder = new TradeBuilder();
            Player playerOne = new Player("a", 100.0);
            Player playerTwo = new Player("b", 150.0);
            Purchasable one = new Purchasable("1", 50.0, Category.BLUE);
            Purchasable two = new Purchasable("2", 10.0, Category.BROWN);
            List<IPurchasable> listOne = new List<IPurchasable>
            {
                one
            };
            List<IPurchasable> listTwo = new List<IPurchasable>
            {
                two
            };
            Trade trade = builder.PlayerOne(playerOne)
                                 .PlayerTwo(playerTwo)
                                 .PlayerOneMoney(10.0)
                                 .PlayerTwoMoney(10.0)
                                 .PlayerOneProperties(listOne)
                                 .PlayerTwoProperties(listTwo)
                                 .Build();
            Assert.IsTrue(trade.GetPlayerOne().Equals(playerOne)
                          && trade.GetPlayerTwo().Equals(playerTwo)
                          && trade.GetPlayerOneTradeMoney() == 10.0
                          && trade.GetPlayerTwoTradeMoney() == 10.0
                          && trade.GetPlayerOneTradeProperty().Equals(listOne)
                          && trade.GetPlayerTwoTradeProperty().Equals(listTwo));
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void TestBuilderException()
        {
            TradeBuilder builder = new TradeBuilder();
            Player playerOne = new Player("a", 100.0);
            Player playerTwo = new Player("b", 150.0);
            Purchasable one = new Purchasable("1", 50.0, Category.BLUE);
            Purchasable two = new Purchasable("2", 10.0, Category.BROWN);
            List<IPurchasable> listOne = new List<IPurchasable>
            {
                one
            };
            List<IPurchasable> listTwo = new List<IPurchasable>
            {
                two
            };

            Trade trade = builder
                          .Build();
        }

        [TestMethod]
        public void TestTrader()
        {
            TradeBuilder builder = new TradeBuilder();
            Player playerOne = new Player("a", 100.0);
            Player playerTwo = new Player("b", 150.0);
            Purchasable one = new Purchasable("1", 50.0, Category.BLUE);
            Purchasable two = new Purchasable("2", 10.0, Category.BROWN);
            List<IPurchasable> listOne = new List<IPurchasable>
            {
                one
            };
            List<IPurchasable> listTwo = new List<IPurchasable>
            {
                two
            };
            Trade trade = builder.PlayerOne(playerOne)
                                 .PlayerTwo(playerTwo)
                                 .PlayerOneMoney(10.0)
                                 .PlayerTwoMoney(10.0)
                                 .PlayerOneProperties(listOne)
                                 .PlayerTwoProperties(listTwo)
                                 .Build();
            Trader trader = new Trader();
            trader.ChangeTrade(trade);
            trader.AcceptTrade();
            Assert.IsTrue(playerOne.GetBalance() == 100.0  && playerTwo.GetBalance() == 150.0);
            Assert.IsTrue(one.GetOwner().Equals(playerTwo) && two.GetOwner().Equals(playerOne));
        }
    }
}
