﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public interface IStockMarket
    {
        void SetNewMarketValue();

        List<Dictionary<Category, Double>> GetStockHistory();

        Dictionary<Category, Double> GetVariation();

        Dictionary<Category, Double> GetMarket();
    }
}
