﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public class TradeBuilder : ITradeBuilder
    {
        private Player _playerOne;
        private Player _playerTwo;
        private List<IPurchasable> _purchasablesOne;
        private List<IPurchasable> _purchasablesTwo;
        private double _moneyOne;
        private double _moneyTwo;

        public Trade Build()
        {
            
            if (_playerOne.Equals(null) || _playerTwo.Equals(null))
            {
                throw new ArgumentException("Trade must have the players involved to be created.");
            }
            if (this._purchasablesOne == null)
            {
                this._purchasablesOne = new List<IPurchasable>();
            }
            if (this._purchasablesTwo == null)
            {
                this._purchasablesTwo = new List<IPurchasable>();
            }
            return new Trade(this._playerOne, this._playerTwo, this._purchasablesOne, this._purchasablesTwo, this._moneyOne, this._moneyTwo);
        }

        public ITradeBuilder PlayerOne(Player player)
        {
            this._playerOne = player;
            return this;
        }

        public ITradeBuilder PlayerOneMoney(double money)
        {
            this._moneyOne = money;
            return this;
        }

        public ITradeBuilder PlayerOneProperties(List<IPurchasable> purchasables)
        {
            this._purchasablesOne = purchasables;
            return this;
        }

        public ITradeBuilder PlayerTwo(Player player)
        {
            this._playerTwo = player;
            return this;
        }

        public ITradeBuilder PlayerTwoMoney(double money)
        {
            this._moneyTwo = money;
            return this;
        }

        public ITradeBuilder PlayerTwoProperties(List<IPurchasable> purchasables)
        {
            this._purchasablesTwo = purchasables;
            return this;
        }
    }
}
