﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public class Purchasable : IPurchasable
    {

        private string _name;
        private double _price;
        private double _stockValue;
        private Category _category;
        private Player _owner;

        public Purchasable(string name, double price, Category category)
        {
            this._name = name;
            this._price = price;
            this._stockValue = 1.0;
            this._category = category;
        }

        public Category GetCategory()
        {
            return this._category;
        }

        public string GetName()
        {
            return this._name;
        }

        public Player GetOwner()
        {
            return this._owner;
        }

        public double GetPrice()
        {
            return this._price;
        }

        public double GetVariation()
        {
            return this._stockValue;
        }

        public void SetOwner(Player player)
        {
            this._owner = player;
        }

        public void SetPrice()
        {
            this._price *= this._stockValue;
        }

        public void SetVariation(double value)
        {
            this._stockValue = value;
            this.SetPrice();
        }
    }
}
