﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public class Player
    {
        private readonly string _name;
        private double _balance;
        private List<IPurchasable> _properties;

        public Player(string name, double balance)
        {
            this._name = name;
            this._balance = balance;
            this._properties = new List<IPurchasable>();
        }

        public string GetName()
        {
            return this._name;
        }

        public double GetBalance()
        {
            return this._balance;
        }

        public List<IPurchasable> GetProperties()
        {
            return this._properties;
        }

        public void AddMoney(double toAdd)
        {
            this._balance += toAdd;
        }

        public void AddProperty(IPurchasable property)
        {
            this._properties.Add(property);
        }

        public void RemoveProperty(IPurchasable property)
        {
            if (this._properties.Contains(property))
            {
                this._properties.Remove(property);
            }
        }
    }
}
