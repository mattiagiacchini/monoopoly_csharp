﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public class StockMarket : IStockMarket
    {
        private Dictionary<Category, Double> _actualMarket;
        private readonly List<Dictionary<Category, Double>> _stockHistory;
        private readonly IList<IPurchasable> _purchasables;
        private readonly Random _random;
        private readonly IList<Category> _purchasableCategories;

        const double RANDOM_MEAN = 100.0;
        const double RANDOM_DEVIATION = 10.0;
        const double RANDOM_DIVIDER = 100.0;
        const double INIT_QUOTATION = 1.0;
        const double MAX_PERCENT = 250.0;
        const double MIN_PERCENT = 5.0;

        /** using an ISet insteand of a translation of Table in order to not translate code
         * I didn't create or didn't use so much extensively.
         * So, the translation of the class will be affected by this choice */
        public StockMarket(List<IPurchasable> purchasables)
        {
            this._purchasables = purchasables;
            _purchasableCategories = new List<Category>
            {
                Category.BLUE,
                Category.BROWN,
                Category.GREEN,
                Category.LIGHT_BLUE,
                Category.ORANGE,
                Category.PINK,
                Category.RED,
                Category.SOCIETY,
                Category.STATION,
                Category.YELLOW
            };
            this._stockHistory = new List<Dictionary<Category, double>>();
            this._actualMarket = InitMarket();
            this._random = new Random();
        }

        private Dictionary<Category, double> InitMarket()
        {
            this._actualMarket = new Dictionary<Category, double>();
            foreach (Category category in this._purchasableCategories)
            {
                this._actualMarket.Add(category, INIT_QUOTATION);
            }
            foreach (IPurchasable purchasable in this._purchasables)
            {
                purchasable.SetVariation(this._actualMarket[purchasable.GetCategory()]);
            }
            this._stockHistory.Add(this._actualMarket);
            return this._actualMarket;
        }

        public Dictionary<Category, double> GetMarket()
        {
            return this._stockHistory[this._stockHistory.Count - 1];
        }

        public List<Dictionary<Category, double>> GetStockHistory()
        {
            return this._stockHistory;
        }

        public Dictionary<Category, double> GetVariation()
        {
            if (this._stockHistory.Count < 2)
            {
                throw new ArgumentException("Stock Market hasn't got two generations yet.");
            }
            Dictionary<Category, Double> toReturn = new Dictionary<Category, double>();
            foreach (Category category in this._purchasableCategories)
            {
                toReturn.Add(category, 100 * (this._stockHistory[this._stockHistory.Count - 1][category]
                        - this._stockHistory[this._stockHistory.Count - 2][category]));
            }
            return toReturn;
        }

        public void SetNewMarketValue()
        {
            this._actualMarket = new Dictionary<Category, double>();
            foreach (Category category in this._purchasableCategories)
            {
                this._actualMarket.Add(category, GetPercentile());
            }
            foreach (IPurchasable purchasable in this._purchasables)
            {
                purchasable.SetVariation(this._actualMarket[purchasable.GetCategory()]);
            }
            this._stockHistory.Add(this._actualMarket);
        }
        /**
         * I'm not translating java code directly into c#, because an equivalent to Random.nextGaussian() is not available in c#.
         * So, I used a Box-Muller transform to generate numbers in a normal distribution (code found on StackOverflow).*/
        private double GetPercentile()
        {
            double u1 = 1.0 - _random.NextDouble(); //uniform(0,1] random doubles
            double u2 = 1.0 - _random.NextDouble();
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                         Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
            double toReturn =
                         RANDOM_MEAN + RANDOM_DEVIATION * randStdNormal; //random normal(mean,stdDev^2)
            return toReturn;
        }
    }
}
