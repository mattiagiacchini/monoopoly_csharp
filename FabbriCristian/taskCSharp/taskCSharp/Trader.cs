﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public class Trader : ITrader
    {
        private Player _playerOne;
        private Player _playerTwo;
        private Trade? _trade;

        public Trader()
        {

            this._trade = null;
        }

        public void AcceptTrade()
        {
            if (this._trade.HasValue)
            {
                this.SwapAlgorithn();
            } else
            {
                return;
            }
        }

        private void SwapAlgorithn()
        {
            List<IPurchasable> listOne = this._trade.Value.GetPlayerOneTradeProperty();
            List<IPurchasable> listTwo = this._trade.Value.GetPlayerTwoTradeProperty();
            double moneyOne = this._trade.Value.GetPlayerOneTradeMoney();
            double moneyTwo = this._trade.Value.GetPlayerTwoTradeMoney();
            foreach (IPurchasable purc in listOne){
                purc.SetOwner(_playerTwo);
            }
            foreach (IPurchasable purc in listTwo)
            {
                purc.SetOwner(_playerOne);
            }
            this._playerOne.AddMoney(-moneyOne);
            this._playerOne.AddMoney(moneyTwo);
            this._playerTwo.AddMoney(-moneyTwo);
            this._playerTwo.AddMoney(moneyOne);
        }

        public void ChangeTrade(Trade? trade)
        {
            this._trade = trade;
            this._playerOne = trade.Value.GetPlayerOne();
            this._playerTwo = trade.Value.GetPlayerTwo();
        }

        public ITrade GetTrade()
        {
            return this._trade.Value;
        }
    }
}
