﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public interface ITrade
    {
        Player GetPlayerOne();

        Player GetPlayerTwo();

        List<IPurchasable> GetPlayerOneTradeProperty();

        List<IPurchasable> GetPlayerTwoTradeProperty();

        double GetPlayerOneTradeMoney();

        double GetPlayerTwoTradeMoney();
    }
}
