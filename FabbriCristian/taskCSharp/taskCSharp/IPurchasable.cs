﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public interface IPurchasable
    {
        string GetName();
        Category GetCategory();
        void SetVariation(double value);
        void SetPrice();
        double GetPrice();
        double GetVariation();
        void SetOwner(Player player);
        Player GetOwner();
    }

    public enum Category
    {
        START,
        JAIL,
        GO_TO_JAIL,
        FREE_PARKING,
        BROWN,
        LIGHT_BLUE,
        PINK,
        ORANGE,
        RED,
        YELLOW,
        GREEN,
        BLUE,
        UNEXPECTED,
        PROBABILITY,
        CALAMITY,
        SOCIETY,
        STATION
    }
}