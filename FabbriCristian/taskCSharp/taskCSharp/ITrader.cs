﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    interface ITrader
    {
        ITrade GetTrade();

        void AcceptTrade();

        void ChangeTrade(Trade? trade);
    }
}
