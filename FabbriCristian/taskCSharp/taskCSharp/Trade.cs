﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    /** Trade is a struct instead of a class in order to be used as a Nonnullable type as a generic type for Nullable<T> and create better translation
     * for Trader and ITrader*/
    public struct Trade : ITrade
    {

        private readonly Player _playerOne;
        private readonly Player _playerTwo;
        private readonly List<IPurchasable> _propertiesOne;
        private readonly List<IPurchasable> _propertiesTwo;
        private readonly double _moneyOne;
        private readonly double _moneyTwo;

        public Trade(Player playerOne, Player playerTwo, List<IPurchasable> propertiesOne, List<IPurchasable> propertiesTwo, double moneyOne, double moneyTwo)
        {
            this._playerOne = playerOne;
            this._playerTwo = playerTwo;
            this._propertiesOne = propertiesOne;
            this._propertiesTwo = propertiesTwo;
            this._moneyOne = moneyOne;
            this._moneyTwo = moneyTwo;
        }

        public Player GetPlayerOne()
        {
            return this._playerOne;
        }

        public double GetPlayerOneTradeMoney()
        {
            return this._moneyOne;
        }

        public List<IPurchasable> GetPlayerOneTradeProperty()
        {
            return this._propertiesOne;
        }

        public Player GetPlayerTwo()
        {
            return this._playerTwo;
        }

        public double GetPlayerTwoTradeMoney()
        {
            return this._moneyTwo;
        }

        public List<IPurchasable> GetPlayerTwoTradeProperty()
        {
            return this._propertiesTwo;
        }
    }
}
