﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taskCSharp
{
    public interface ITradeBuilder
    {
        ITradeBuilder PlayerOne(Player player);

        ITradeBuilder PlayerTwo(Player player);

        ITradeBuilder PlayerOneProperties(List<IPurchasable> purchasables);

        ITradeBuilder PlayerTwoProperties(List<IPurchasable> purchasables);

        ITradeBuilder PlayerOneMoney(double money);

        ITradeBuilder PlayerTwoMoney(double money);

        Trade Build();
    }
}
